---
title: Languages and Frameworks
description: An overview of the Languages and Frameworks we use at Grey Software
category: Tools and Tech
position: 19
---

## Vue

![vue Preview](/tech-stack/vue-preview.png)

Vue is a powerful Javascript framework that integrates well with classic web development tools and is a joy to learn and work with.

<cta-button link="https://vuejs.org" text="Learn More" > </cta-button>

## Nuxt

![Nuxt Preview](/tech-stack/nuxt-preview.png)

The website you are currently on is built using nuts. So are many of the other websites throughout our ecosystem.

Nuxt has made it incredibly simple for us to develop flexible and performant VueJS websites with very limited resources. When students come to us asking what framework they should use to develop their websites, we don't hesitate before recommending Nuxt.

<cta-button link="https://nuxtjs.org" text="Learn More" > </cta-button>

## Quasar

![Quasar Preview](/tech-stack/quasar-preview.png)

Quasar is our go to framework for

<cta-button link="https://quasar.dev" text="Learn More" > </cta-button>
