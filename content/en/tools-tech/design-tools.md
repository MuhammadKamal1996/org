---
title: Design Tools
description: An overview of the Design Tools we use at Grey Software
category: Tools and Tech
position: 18
---

## Figma

![Figma Preview](/tech-stack/figma-preview.png)

Figma is a web-based graphic and user interface design app. It allows you to collaborate on design files with your team in real time, and fits very well in the toolkit of a frontend web developer.

At Grey Software, we use Figma for our UI design and prototyping work.

<cta-button  link="https://www.figma.com/education/" text="Claim Edu Plan!" > </cta-button>

<cta-button  link="http://figma.grey.software" text="Our Figma File" > </cta-button>

## Overlay

![Overlay Preview](/tech-stack/overlay-preview.png)

Overlay is an exciting piece of technology that allows you to export Figma designs to modern web code. Since were constantly on the lookout for tools that simplify the design to development process, we are delighted to use an advocate for overlay.

<cta-button link="https://overlay-tech.com" text="Learn More" > </cta-button>
